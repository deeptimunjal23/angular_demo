export const environment = {
  production: true,
  BaseURL: 'https://demowork.site/',
  APIURL: 'https://api.recognizr.app/',
};
