// /**
//  *
//  * You can write your JS code here, DO NOT touch the default style file
//  * because it will make it harder for you to update.
//  *
//  */

// //Toggle Sidenav
// $(document).ready(function () {
//     const iconNavbarSidenav = document.getElementById('iconNavbarSidenav');
//     const iconSidenav = document.getElementById('iconSidenav');
//     const sidenav = document.getElementById('sidenav-main');
//     let body = document.getElementsByTagName('body')[0];
//     let className = 'g-sidenav-pinned';

//     if (iconNavbarSidenav) {
//         iconNavbarSidenav.addEventListener("click", toggleSidenav);
//     }

//     if (iconSidenav) {
//         iconSidenav.addEventListener("click", toggleSidenav);
//     }

//     function toggleSidenav() {
//         if (body.classList.contains(className)) {
//             body.classList.remove(className);
//             setTimeout(function () {
//                 sidenav.classList.remove('bg-white');
//             }, 100);
//             sidenav.classList.remove('bg-transparent');

//         } else {
//             body.classList.add(className);
//             sidenav.classList.add('bg-white');
//             sidenav.classList.remove('bg-transparent');
//             iconSidenav.classList.remove('d-none');
//         }
//     }

//     $(document).click(function () {
//         toggleSidenav()
//     })
// })