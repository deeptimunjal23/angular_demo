import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { tap } from 'rxjs/operators';
import { environment } from '../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class UserService {
  isLoggedIn = false;
  private url = environment.APIURL + 'demo';
  private token = '';
  item = localStorage.getItem('access_token');

  constructor(private http: HttpClient) {
    if (this.item != null) {
      if (this.item) {
        this.isLoggedIn = true;
        this.token = this.item;
      }
    }
  }

  getAllUsers(body: any): Observable<any> {
    const url = `${this.url}/pagination`;
    const httpOptions = {
      headers: new HttpHeaders({
        "Access-Control-Allow-Origin": "*",
        'Content-Type': 'application/json',
        "Access-Control-Allow-Credentials": "true",
        "Authorization": this.token
      })
    };
    return this.http.post(url, body, httpOptions);
  }

  addUser(body: any): Observable<any> {
    const url = `${this.url}/create`;
    const httpOptions = {
      headers: new HttpHeaders({
        "Access-Control-Allow-Origin": "*",
        'Content-Type': 'application/json',
        "Access-Control-Allow-Credentials": "true",
        "Authorization": this.token
      })
    };
    return this.http.post(url, body, httpOptions);
  }


  editUser(body: any): Observable<any> {
    const url = `${this.url}/update`;
    const httpOptions = {
      headers: new HttpHeaders({
        "Access-Control-Allow-Origin": "*",
        'Content-Type': 'application/json',
        "Access-Control-Allow-Credentials": "true",
        "Authorization": this.token
      })
    };
    return this.http.post(url, body, httpOptions);
  }

  getUserInfo(body: any): Observable<any> {
    const url = `${this.url}/info`;
    const httpOptions = {
      headers: new HttpHeaders({
        "Access-Control-Allow-Origin": "*",
        'Content-Type': 'application/json',
        "Access-Control-Allow-Credentials": "true",
        "Authorization": this.token
      })
    };
    return this.http.post(url, body, httpOptions);
  }

  deleteUser(body: any): Observable<any> {
    const url = `${this.url}/delete`;
    const httpOptions = {
      headers: new HttpHeaders({
        "Access-Control-Allow-Origin": "*",
        'Content-Type': 'application/json',
        "Access-Control-Allow-Credentials": "true",
        "Authorization": this.token
      })
    };
    return this.http.post(url, body, httpOptions);
  }
}
