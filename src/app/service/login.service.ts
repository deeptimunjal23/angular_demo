import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { tap } from 'rxjs/operators';
import { environment } from '../../environments/environment'
@Injectable({
  providedIn: 'root'
})
export class LoginService {
  isLoggedIn = false;
  private url = environment.APIURL + 'demo';
  private token = '';
  item = localStorage.getItem('access_token');

  constructor(private http: HttpClient) {
    if (this.item != null) {
      if (this.item) {
        this.isLoggedIn = true;
        this.token = this.item;
      }
    }
  }

  userLogin(body: any): Observable<any> {
    const url = `${this.url}/login`;
    const httpOptions = {
      headers: new HttpHeaders({
        "Access-Control-Allow-Origin": "*",
        'Content-Type': 'application/json',
        "Access-Control-Allow-Credentials": "true"
      })
    };
    return this.http.post(url, body, httpOptions
    )
      .pipe(tap((res: any) => {
        this.isLoggedIn = true;
        localStorage.setItem('access_token', res.data['token']);
        localStorage.setItem('currentUser', JSON.stringify(res['data']));
      }));
  }
  userSignUp(body: any): Observable<any> {
    const url = `${this.url}/signup`;
    const httpOptions = {
      headers: new HttpHeaders({
        "Access-Control-Allow-Origin": "*",
        'Content-Type': 'application/json',
        "Access-Control-Allow-Credentials": "true"
      })
    };
    return this.http.post(url, body, httpOptions
    )
      .pipe(tap((res: any) => {
        this.isLoggedIn = true;
        localStorage.setItem('access_token', res['token']);
        localStorage.setItem('currentUser', JSON.stringify(res['data']));
      }));
  }


}
