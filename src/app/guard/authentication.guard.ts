import { Injectable } from '@angular/core';
import { Router, CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, UrlTree } from '@angular/router';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class AuthenticationGuard implements CanActivate {
  redirectUrl: any;
  loginURL = "/signin";
  constructor(private router: Router) { }
  canActivate(
    route: ActivatedRouteSnapshot,
    state: RouterStateSnapshot): Observable<boolean | UrlTree> | Promise<boolean | UrlTree> | boolean | UrlTree {
    const url: string = state.url;
    if (typeof localStorage.currentUser != 'undefined') {
      if (url.includes("/signin")) {
        const userRole = JSON.parse(localStorage.currentUser);
        if (userRole.user_type) {
          this.redirectUrl = "/admin/dashboard";
        } else {
          this.redirectUrl = '/signin';
          localStorage.removeItem('access_token');
          localStorage.removeItem('currentUser');
        }
        this.router.navigate([this.redirectUrl]);
      }
      return true;
    }
    if (url.includes("/signin")) {
      return true;
    }
    this.router.navigate(['/signin']);
    return false;
  }

}
