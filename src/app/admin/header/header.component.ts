import { Component, OnInit, Input } from '@angular/core';
import { DynamicScriptLoaderService } from '../../service/dynamic-script-loader.service';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnInit {

  @Input() page_title = '';
  userData: any = localStorage.getItem('currentUser');
  constructor(private dynamicScriptLoaderService: DynamicScriptLoaderService) {
    if (this.userData) {
      this.userData = JSON.parse(this.userData);
    }
  }

  ngOnInit(): void {
    const sidenav = document.getElementById('sidenav-main');
    let body = document.getElementsByTagName('body')[0];
    let className = 'g-sidenav-pinned';
    body.classList.remove(className);
    setTimeout(function () {
      sidenav!.classList.remove('bg-white');
    }, 100);
    sidenav!.classList.remove('bg-transparent');
  }

  toggleSidenav() {
    const iconSidenav = document.getElementById('iconSidenav');
    const sidenav = document.getElementById('sidenav-main');
    let body = document.getElementsByTagName('body')[0];
    let className = 'g-sidenav-pinned';

    if (body.classList.contains(className)) {
      body.classList.remove(className);
      const sidenav = document.getElementById('sidenav-main');
      setTimeout(function () {
        sidenav!.classList.remove('bg-white');
      }, 100);
      sidenav!.classList.remove('bg-transparent');

    } else {
      body.classList.add(className);
      sidenav!.classList.add('bg-white');
      sidenav!.classList.remove('bg-transparent');
      iconSidenav!.classList.remove('d-none');
    }
  }

}
