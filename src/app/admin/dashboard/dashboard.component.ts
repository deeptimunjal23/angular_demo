import { Component, OnInit } from '@angular/core';
import { DynamicScriptLoaderService } from '../../service/dynamic-script-loader.service';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.css']
})
export class DashboardComponent implements OnInit {
  title = 'Dashboard';
  constructor(private dynamicScriptLoaderService: DynamicScriptLoaderService) { }

  ngOnInit(): void {
    this.dynamicScriptLoaderService.load('chartjs').then(data => {
      this.dynamicScriptLoaderService.load('chart-custom').then(data => {
        this.dynamicScriptLoaderService.load('main').then(data => {
        }).catch(error => console.log(error));
      }).catch(error => console.log(error));
    }).catch(error => console.log(error));
  }

  ngAfterViewInit() {
   
  }
}
