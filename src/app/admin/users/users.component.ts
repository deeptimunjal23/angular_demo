import { Component, OnInit } from '@angular/core';
import { UserService } from '../../service/user.service';
import { FormGroup, FormBuilder } from '@angular/forms';
import { Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
declare var $: any;
@Component({
  selector: 'app-users',
  templateUrl: './users.component.html',
  styleUrls: ['./users.component.css']
})
export class UsersComponent implements OnInit {
  title = 'Users';
  user_items: any = [];
  userPagination: any = [];
  userItemsTotal: any;
  userShowPage: boolean = false;
  userPageOffset: number = 0;
  userCurrentPage: number = 1;
  userData: any = [];
  userPerPage: number = 10;
  userSortBy: string = "user_id";
  userSortOrder: string = "desc";
  searchForm: FormGroup;
  search: string = '';
  total_pages: any;
  activePage: number = 1;
  loading: boolean = true;
  active_user_id: any = 0;
  constructor(public router: Router, public toastrService: ToastrService, private fb: FormBuilder, private userService: UserService) {
    this.searchForm = this.fb.group({
      search: '',
    });
  }

  ngOnInit(): void {
    this.loadUsers();
  }

  loadUsers() {
    const body = { limit: this.userPerPage, offset: this.userPageOffset, sortby: this.userSortBy, sort: this.userSortOrder, search: this.search };
    this.userService.getAllUsers(body).subscribe((result: any) => {
      this.userData = result.page;
      this.userItemsTotal = result.total_records;
      this.userPagination['from'] = this.userPageOffset + 1;
      this.total_pages = result.total_pages;
      if (this.activePage != 1) {
        if (this.activePage == result.total_pages) {
          this.userPagination['to'] = this.userItemsTotal;
        } else {
          this.userPagination['to'] = this.userPageOffset + this.userPerPage;
        }
      } else {
        this.userPagination['to'] = this.userData.length;
      }
      if (this.userItemsTotal <= this.userPerPage) {
        this.userShowPage = false;
      } else {
        this.userShowPage = true;
      }
      this.loading = false;
    }, err => {
      this.loading = false;
      this.handleError(err);
    });
  }

  handleError(err: any) {
    if (err.error.message) this.toastrService.error(err.error.message);
    if (err.status == 401) {
      localStorage.removeItem('access_token');
      localStorage.removeItem('currentUser');
      window.location.reload();
    }
  }

  counter(i: number) {
    return new Array(i);
  }

  OnClickPage(i: number) {
    if (i <= 0 || i > this.total_pages) {
      return
    } else {
      this.activePage = i;
      this.userPageOffset = this.userPerPage * (i - 1);
      this.loadUsers();
    }
  }

  sortBy(sort: any) {
    this.userSortBy = sort;
    if (this.userSortOrder == "desc") {
      this.userSortOrder = "asc";
    } else {
      this.userSortOrder = "desc";
    }
    this.loadUsers();
  }

  searchKey(event: any) {
    const formModel = this.searchForm.value;
    this.search = formModel.search.trim();
    this.userCurrentPage = 1;
    this.userPageOffset = 0;
    this.loadUsers();
  }

  trackById(index: number, item: any) {
    return item.id
  }

  deleteConfirmation(id: number) {
    this.active_user_id = id;
  }

  finalDelete() {
    const body = { user_id: this.active_user_id };
    this.userService.deleteUser(body).subscribe(result => {
      if (result.message) this.toastrService.success(result.message);
      this.eventFire(document.getElementById('delete_alert_close'), 'click');
      this.loadUsers();
    }, err => {
      this.eventFire(document.getElementById('delete_alert_close'), 'click');
      this.handleError(err);
    });
  }

  eventFire(el: any, etype: any) {
    if (el.fireEvent) {
      el.fireEvent('on' + etype);
    } else {
      var evObj = document.createEvent('Events');
      evObj.initEvent(etype, true, false);
      el.dispatchEvent(evObj);
    }
  }
}
