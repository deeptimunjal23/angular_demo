import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators, FormControl, AbstractControl } from '@angular/forms';
import { Router, ActivatedRoute } from '@angular/router';
import { UserService } from '../../../service/user.service';
import { ToastrService } from 'ngx-toastr';
declare var $: any;
const EMAIL_REGEX = /^[\w-\.]+@([\w-]+\.)+[\w-]{2,4}$/;

@Component({
  selector: 'app-add-edit-user',
  templateUrl: './add-edit-user.component.html',
  styleUrls: ['./add-edit-user.component.css']
})
export class AddEditUserComponent implements OnInit {
  title = 'Add User';
  userAddEditForm: FormGroup;
  isAddMode: boolean = true;
  id: number = 0;
  loading: boolean = false;
  submitted: boolean = false;
  constructor(public toastrService: ToastrService, public router: Router, private userService: UserService, private route: ActivatedRoute) {
    this.userAddEditForm = new FormGroup({
      name: new FormControl('', [Validators.required]),
      email: new FormControl('', [Validators.required, Validators.pattern(EMAIL_REGEX)]),
      position_title: new FormControl('', [Validators.required]),
      company: new FormControl('')
    });
  }

  MatchValidator(frm: AbstractControl) {
    return frm.get('password')!.value === frm.get('confirm_password')!.value
      ? null : { 'mismatch': true };
  }

  get f() {
    return this.userAddEditForm.controls;
  }

  ngOnInit(): void {
    this.id = this.route.snapshot.params['id'];
    this.isAddMode = !this.id;
    if (!this.isAddMode) {
      this.loadEditData();
      this.title = 'Edit User'
    }

    setTimeout(() => {
      document.getElementById('usersId')!.classList.add('active');
    }, 200);
  }

  loadEditData() {
    const body = { user_id: this.id };
    this.userService.getUserInfo(body).subscribe(result => {
      this.userAddEditForm.setValue({
        name: result.data.name,
        email: result.data.email,
        position_title: result.data.position_title,
        company: result.data.company,
      });
    }, err => {
      this.handleError(err);
    });
  }

  handleError(err: any) {
    if (err.error.message) this.toastrService.error(err.error.message);
    if (err.status == 401) {
      localStorage.removeItem('access_token');
      localStorage.removeItem('currentUser');
      window.location.reload();
      this.router.navigate(['/login']);
    }
  }

  onSubmit() {
    this.loading = true;
    if (this.userAddEditForm.invalid) {
      this.loading = false;
      this.submitted = true;
      return;
    } else {
      if (this.isAddMode) {
        this.saveUser();
      } else {
        this.updateUser();
      }
    }
  }

  saveUser() {
    this.userService.addUser(this.userAddEditForm.value).subscribe(result => {
      this.loading = false;
      if (result.message) this.toastrService.success(result.message);
      this.router.navigate(['/admin/users']);
    }, err => {
      this.loading = false;
      this.handleError(err);
    });
  }

  updateUser() {
    this.userAddEditForm.value['user_id'] = this.id
    this.userService.editUser(this.userAddEditForm.value).subscribe(result => {
      this.loading = false;
      if (result.message) this.toastrService.success(result.message);
      this.router.navigate(['/admin/users']);
    }, err => {
      this.loading = false;
      this.handleError(err);
    });
  }

}
