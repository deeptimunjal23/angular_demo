import { Component, OnInit } from '@angular/core';
import { FormGroup, Validators, FormControl } from '@angular/forms';
import { LoginService } from '../service/login.service';
import { Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';

const EMAIL_REGEX = /^[\w-\.]+@([\w-]+\.)+[\w-]{2,4}$/;
@Component({
  selector: 'app-sign-up',
  templateUrl: './sign-up.component.html',
  styleUrls: ['./sign-up.component.css']
})
export class SignUpComponent implements OnInit {
  SignUpForm: FormGroup;
  submitted: boolean = false;
  isDisable: boolean = false;
  loading: boolean = false;
  constructor(private loginService: LoginService, public router: Router, private toastrService: ToastrService) {

    this.SignUpForm = new FormGroup({
      name: new FormControl('', [Validators.required]),
      email: new FormControl('', [Validators.required, Validators.pattern(EMAIL_REGEX)]),
      password: new FormControl('', [Validators.required, Validators.minLength(6), Validators.maxLength(20)])
    });

  }

  get f() {
    return this.SignUpForm.controls;
  }


  onSubmit() {
    this.isDisable = true;
    if (this.SignUpForm.invalid) {
      this.submitted = true;
      this.isDisable = false;
      return;
    }
    this.signup(this.SignUpForm.value)
  }

  ngOnInit(): void {
  }

  signup(value: any) {
    value['user_type'] = 2;
    this.loginService.userSignUp(value).subscribe((result: any) => {
      this.isDisable = false;
      this.loginService.isLoggedIn = true;
      localStorage.setItem('access_token', result.data['token']);
      localStorage.setItem('currentUser', JSON.stringify(result['data']));
      var redirectUrl;
      redirectUrl = "/admin/dashboard";
      this.loading = false;
      this.router.navigate([redirectUrl]);
      this.toastrService.success(result.message);
    }, err => {
      this.isDisable = false;
      this.toastrService.error(err.error.message);
    });
  }

}
