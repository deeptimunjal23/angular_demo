import { Component, OnInit } from '@angular/core';
import { FormGroup, Validators, FormControl } from '@angular/forms';
import { LoginService } from '../service/login.service';
import { Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
const EMAIL_REGEX = /^[\w-\.]+@([\w-]+\.)+[\w-]{2,4}$/;
@Component({
  selector: 'app-sign-in',
  templateUrl: './sign-in.component.html',
  styleUrls: ['./sign-in.component.css']
})
export class SignInComponent implements OnInit {
  loginForm: FormGroup;
  submitted: boolean = false;
  loading: boolean = false;
  constructor(private loginService: LoginService, public router: Router, private toastrService: ToastrService) { 
    this.loginForm = new FormGroup({
      email: new FormControl('', [Validators.required, Validators.pattern(EMAIL_REGEX)]),
      password: new FormControl('', [Validators.required])
    });
  }

  get f() {
    return this.loginForm.controls;
  }

  ngOnInit(): void {
  }

  onSubmit() {
    this.loading = true;
    if (this.loginForm.invalid) {
      this.submitted = true;
      this.loading = false;
      return;
    }
    this.login(this.loginForm.value)
  }

  login(value:any) {
    value['user_type'] = 2;
    this.loginService.userLogin(value).subscribe(result => {
      this.loading = false;
      if (result.message) this.toastrService.success(result.message); 
      if (this.loginService.isLoggedIn) {
        this.router.navigate(['/admin/dashboard']);
      }
    }, err => {
      this.loading = false;
      if (err.error.message) this.toastrService.error(err.error.message);
    });
  }


}
